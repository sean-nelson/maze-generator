(defproject maze-generator "1.0"
  :description "A Quil sketch illustrating an implementation of Prim's algorithm for maze generation"
  :url "https://gitlab.com/sean-nelson/maze-generator"
  :license {:name "BSD License 2.0"
            :url "https://spdx.org/licenses/BSD-3-Clause.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [quil "3.1.0"]])
