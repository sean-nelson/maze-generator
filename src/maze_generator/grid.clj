(ns maze-generator.grid
  (:require [maze-generator.conf :as conf]))

(defn int->coord [n]
  [(quot n conf/row-length)
   (mod n conf/row-length)])

(defn coord->int [r c]
  (+ (* r conf/row-length) c))

(defn in-bounds? [r c]
  (and (< r conf/row-length)
       (< c conf/row-length)
       (>= r 0)
       (>= c 0)))

(defn get-neighbors [n]
  (let [[r c] (int->coord n)
        neighbors {:bottom [(+ r 1) c]
                   :right [r (+ c 1)]
                   :top [(- r 1) c]
                   :left [r (- c 1)]}]
    (reduce (fn [new-map [k v]]
              (assoc new-map k (apply coord->int v)))
            {}
            (filter #(apply in-bounds? (second %)) neighbors))))
