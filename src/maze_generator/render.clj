(ns maze-generator.render
  (:require [maze-generator.graph :as gph]
            [maze-generator.grid :as grid]
            [quil.core :as q]
            [maze-generator.conf :as conf]))

; render functions
(defn draw-cell [{graph :graph :as _state} n]
  (q/fill 255)
  (q/stroke-weight 3)
  (q/stroke 0)
  (let [[r c] (grid/int->coord n)
        cell-width (/ conf/board-size conf/row-length)
        origin-point [(* c cell-width)
                      (* r cell-width)]
        neighbors (grid/get-neighbors n)]
    ;draw borders
    (q/with-translation origin-point
      ; top segment
      (when-not (gph/connected? graph n (:top neighbors))
        (q/line 0 0 cell-width 0))
      ; right segment
      (when-not (gph/connected? graph n (:right neighbors))
        (q/line cell-width 0 cell-width cell-width))
      ; bottom segment
      (when-not (gph/connected? graph n (:bottom neighbors))
        (q/line cell-width cell-width 0 cell-width))
      ; left segment
      (when-not (gph/connected? graph n (:left neighbors))
        (q/line 0 cell-width 0 0)))))

(defn draw-frontier [{graph :graph :as _state}]
  (let [frontier (gph/get-frontier graph)]
    (doseq [cell frontier]
      (let [[r c] (grid/int->coord cell)
            cell-width (/ conf/board-size conf/row-length)
            origin-point [(* c cell-width)
                          (* r cell-width)]]
        (q/fill 150 0 0 150)
        (q/with-translation origin-point
          (q/rect 0 0 cell-width cell-width))))))

(defn draw-state [state]
  (q/background 255)
  (q/with-translation [conf/margin conf/margin]
    (doseq [n (range (* conf/row-length conf/row-length))]
      (draw-cell state n))
    (draw-frontier state)))
