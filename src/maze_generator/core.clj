(ns maze-generator.core
  (:require [maze-generator.conf :as conf]
            [maze-generator.graph :as gph]
            [maze-generator.render :as rnd]
            [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  ; Set frame rate to 30 frames per second.
  (q/frame-rate 30)
  ; graph is represented as an adjacency list
  {:graph (gph/generate-initial-graph)})

(defn update-state [state]
  (if (= (* conf/row-length conf/row-length) (count (keys (:graph state))))
    (do
      (q/no-loop)
      state)
    (assoc state :graph (gph/grow-maze (:graph state)))))

(q/defsketch maze-generator
  :title "Maze generator"
  :size (repeat 2 (+ conf/board-size (* 2 conf/margin)))
  ; setup function called only once, during sketch initialization.
  :setup setup
  ; update-state is called on each iteration before draw-state.
  :update update-state
  :draw rnd/draw-state
  :features [:keep-on-top]
  ; This sketch uses functional-mode middleware.
  ; Check quil wiki for more info about middlewares and particularly
  ; fun-mode.
  :middleware [m/fun-mode])
