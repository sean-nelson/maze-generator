(ns maze-generator.graph
  (:require [maze-generator.conf :as conf]
            [maze-generator.grid :as grd]))

; cells in the grid will be represented by an integer
; zero-indexed starting in the top left corner and wrapping
; at the end of a row
;
; 0 1 2
; 3 4 5
; 6 7 8

(defn connected? [graph a b]
  (not (nil? (some (partial = b) (get graph a)))))

(defn generate-initial-graph
  "Returns the initial state for the maze graph. Which will be a single cell not connected to any others"
  []
  {(rand-int (* conf/row-length conf/row-length)) []})

(defn add-connection [graph a b]
  (-> graph
      (assoc a (vec (conj (get graph a) b)))
      (assoc b (vec (conj (get graph b) a)))))

(defn get-frontier
  "Returns the complete list of cells which border the current spanning tree"
  [graph]
  (->> (keys graph)
       (map grd/get-neighbors)
       (map vals)
       flatten
       distinct
       (filter (complement #(some #{%} (keys graph))))))

(defn grow-maze
  "Chooses a random cell from the frontier of `graph` and adds a connection to it"
  [graph]
  (let [frontier (get-frontier graph)
        new-cell (nth frontier (rand-int (count frontier)))
        neighbors-in-graph (filter #(some #{%} (keys graph)) (-> (grd/get-neighbors new-cell)
                                                                 vals))
        connected-neighbor (nth neighbors-in-graph (rand-int (count neighbors-in-graph)))]
    (add-connection graph connected-neighbor new-cell)))
